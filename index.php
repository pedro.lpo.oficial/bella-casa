<!DOCTYPE html>
<html lang="pt">
<head>
    <link rel="icon" href="IMG/icon/Icon.svg" type="image/icon type">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bella Casa - Moveis Planejados</title>
    <link rel="stylesheet" href="CSS/general.css">
    <link rel="stylesheet" href="CSS/header.css">
    <link rel="stylesheet" href="CSS/main.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<body>
    <header>
        <div id="hamburguer">
            <div></div>
            <div></div>
            <div></div>
        </div>
        <a href="#home"><img src="IMG/icon/Logo v1.svg"/></a>
        <ul>
            <li><a href="#about">Nós</a></li>
            <li><a href="#product">Produtos</a></li>
            <li><a href="#contact">Contato</a></li>
            <li><a href="#contact">Orçamento</a></li>
        </ul>
        <a class="button btWhite" href="#contact">
            <p>Fazer Orçamento</p>
        </a>
    </header>

    
    <main>

        <section id="menuMobile" style="display: none;">
            <a id="imgMenuMobile" href="#home"><img src="IMG/icon/Logo v1.svg"/></a>
                <ul id="optionMenuMobile">
                    <li><a href="#about">Nós</a></li>
                    <li><a href="#product">Produtos</a></li>
                    <li><a href="#contact">Contato</a></li>
                    <li><a href="#contact">Orçamento</a></li>
                </ul>
            <p id="exitMenuMobile">Sair</p>
        </section>

        <div id="floatButton">
            <a href=""><img src="IMG/icon/WhatsApp.svg" alt=""/></a>
            <a href="#home"><img src="IMG/icon/ArrowUp.svg" alt=""/></a>
        </div>

        <section id="home">
            <div class="slider1 slider1-1 fade">
                
                    <div>
                        <h1 >Seu escritório<br>do seu jeito!</h1>
                        <a class="button btWhite" href="#apresentation">
                            <p>Descubra</p>
                        </a>
                    </div>
                    <div>
                        <img src="IMG/Background1--1.png" alt="">
                    </div>
                
            </div>
            <div class="slider1 slider1-2 fade">
                
                    <div>
                        <h1 >O que seu<br> lar pode ser...</h1>
                        <a class="button btWhite" href="#apresentation">
                            <p>Descubra</p>
                        </a>
                    </div>
                    <div>
                        <img src="IMG/Background1--2.png" alt="">
                    </div>
                
            </div>
            <div class="slider1 slider1-3 fade">
                
                    <div>
                        <h1 >Relaxe e curta<br> a SUA vista!</h1>
                        <a class="button btWhite" href="#apresentation">
                            <p>Descubra</p>
                        </a>
                    </div>
                    <div>
                        <img src="IMG/Background1--3.png" alt="">
                    </div>
                
            </div>
            <div class="sliderSteps">
                <div class="dot dot1" onclick="fSlider1(1)"></div>
                <div class="dot dot1" onclick="fSlider1(2)"></div>
                <div class="dot dot1" onclick="fSlider1(3)"></div>
            </div>
        </section>


        <section id="about">
            <p class="subTittle">Sobre a</p>
            <p class="tittle">Bella Casa.</p>
            <div>
                <div class="topic">
                    <div>
                        <div>
                            <div><p class="tittleTxt">Missão</p></div>
                            <p class="txt">Entregar com excelência os produtos solicitados pelos 
                                nossos clientes, criar ambientes confortáveis e atrativos e comunicar 
                                de forma singular o que o cliente pensa a respeito do ambiente.</p>
                        </div>
                    </div>
                    <div>
                        <div>
                            <div><p class="tittleTxt">Visão</p></div>
                            <p class="txt">Ser o principal fornecedor de móveis planejados de
                                 Brasília-DF e Goiás.</p>
                        </div>
                    </div>
                    <div>
                        <div>
                            <div><p class="tittleTxt">Valores</p></div>
                            <p class="txt">Honestidade, agilidade, competência e cumprir expectativas.</p>
                        </div>
                    </div>
                </div>
                <div><p class="txt">Criamos móveis planejados para qualquer ambiente ou necessidade. 
                    Com eficiência nos comprometemos a atingir as metas de nossos clientes e 
                    proporcionar a melhor experiência possível, desde o orçamento até a entrega 
                    do projeto.</p></div>
            </div>
        </section>

        <section id="news">
            <div>
                <p class="tittleTxt">Receba as nossas novidades por e-mail.</p>
                <form action="GET">
                    <input type="text" placeholder="Seu e-mail aqui" required>
                    <input class="button btBlue" type="submit" name="newsEmail" id="newsEmail" value="Receber Novidades">
                </form>
            </div>
        </section>

        <section id="apresentation">
            <div class="slider2 slider2-1 fade">
                <div>
                    <div>
                        <p class="subTittle">Sobre a</p>
                        <p class="tittle">Bella Casa.</p>
                    </div>
                    <div>
                        <div><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum
                            dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet,
                            consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                        <a class="button btWhite" href="#contact">Fazer Orçamento</a>
                    </div>
                </div>
            </div>
            <div class="slider2 slider2-2 fade">
                <div>
                    <div>
                        <p class="subTittle">Sobre a</p>
                        <p class="tittle">Bella Casa.</p>
                    </div>
                    <div>
                        <div><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum
                            dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet,
                            consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                        <a class="button btWhite" href="#contact">Fazer Orçamento</a>
                    </div>
                </div>
            </div>
            <div class="slider2 slider2-3 fade">
                <div>
                    <div>
                        <p class="subTittle">Sobre a</p>
                        <p class="tittle">Bella Casa.</p>
                    </div>
                    <div>
                        <div><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum
                            dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet,
                            consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                        <a class="button btWhite" href="#contact">Fazer Orçamento</a>
                    </div>
                </div>
            </div>
            
            <div class="sliderSteps">
                <div class="dot dot2" onclick="fSlider2(1)"></div>
                <div class="dot dot2" onclick="fSlider2(2)"></div>
                <div class="dot dot2" onclick="fSlider2(3)"></div>
            </div>
        </section>

        <section id="info">
            <p class="subTittle">Moveis</p>
            <p class="tittle">Planejados</p>
            <div>
                <div class="topic">
                    <div>
                        <div>
                            <div><p class="tittleTxt">Planejamento</p></div>
                            <p class="txt">Desenvolvemos o SEU sonho e o tornamos NOSSO. Aperfeiçoamos ideias e criamos um ambiente perfeito: do seu jeito! </p>
                        </div>
                    </div>
                    <div>
                        <div>
                            <div><p class="tittleTxt">Produção</p></div>
                            <p class="txt">Transformando algo intocável em perfeitamente usual. Seja uma mesa, bancada, armário, painel... criamos para você!</p>
                        </div>
                    </div>
                    <div>
                        <div>
                            <div><p class="tittleTxt">Entrega</p></div>
                            <p class="txt">Rápido e do seu jeito, este é o nosso modelo de produção!</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div>
                        <img src="IMG/icon/iCoroa.svg">
                        <p>Excelência</p>
                    </div>
                    <div>
                        <img src="IMG/icon/iTempo.svg">
                        <p>Prazo</p>
                    </div>
                    <div>
                        <img src="IMG/icon/iChat.svg">
                        <p>Acessível</p>
                    </div>
                </div>
            </div>
        </section>

        <section id="product">
            <div>
                <div>
                    <div>
                        <div class="slider3 fade">
                            <div class="slider31 fade"></div>
                            <div class="slider31 fade"></div>
                            <div class="slider31 fade"></div>
                            <div class="sliderSteps">
                                <div class="dot dot31" onclick="fSlider31(1)"></div>
                                <div class="dot dot31" onclick="fSlider31(2)"></div>
                                <div class="dot dot31" onclick="fSlider31(3)"></div>
                            </div>
                        </div>
                        <div class="slider3 fade">
                            <div class="slider32 fade"></div>
                            <div class="slider32 fade"></div>
                            <div class="sliderSteps">
                                <div class="dot dot32" onclick="fSlider32(1)"></div>
                                <div class="dot dot32" onclick="fSlider32(2)"></div>
                            </div>
                        </div>
                        <div class="slider3 fade">
                            <div class="slider33 fade"></div>
                            <div class="slider33 fade"></div>
                            <div class="sliderSteps">
                                <div class="dot dot33" onclick="fSlider33(1)"></div>
                                <div class="dot dot33" onclick="fSlider33(2)"></div>
                            </div>
                        </div>
                    </div>
                    <div>
                        
                        <button value="1" id="product1" onClick="fP1()">
                            <div>
                                <div>
                                    <p class="subTittle">Para a</p>
                                    <p class="tittle">Cozinha</p>
                                    <p>Redecore, redistribua e renove o ambiente mais importante de uma
                                         casa: a cozinha! Deixe do seu jeito, seja mais rústico ou
                                          conteporâneo, o foco é te deixar confortável enquanto prepara
                                           aquela deliciosa refeição para a família ou amigos. Faça um
                                            orçamento e se surpreenda com as possibilidades!</p>
                                </div>
                            </div>
                        </button>
                        
                        <button value="0" id="product2" onClick="fP2()">
                            <div>   
                                <div>
                                    <p class="subTittle">Para o</p>
                                    <p class="tittle">Escritório</p>
                                    <p>Seja em casa ou não, todos concordam que um bom ambiente de
                                         trabalho começa com bons móveis que te ajudam no dia-a-dia.
                                          Algo sofisticado e que transpareça o que sua empresa significa
                                           para os colaboradores e seus clientes, além de ser durável e
                                            elegante. Faça um orçamento e melhore seu tempo trabalhando!</p>
                                </div>
                            </div>
                        </button>
                        <button value="0" id="product3" onClick="fP3()">
                            <div>
                                <div>
                                    <p class="tittle">Outros</p>
                                    <p>Ainda não encontrou o que procurava? Não se preocupe,
                                        fabricamos todo tipo de móveis para você, sua empresa
                                        ou para alguém que você ama. Entre em contato conosco!</p>
                                </div>
                            </div>
                        </div>
                    </button>
                </div>
                <div>
                    <div>
                        <a class="button btWhite" href="#contact">Fazer Orçamento</a>
                    </div>
                </div>
            </div>
        </section>

        <section id="contact">
            <div>
                <p class="tittle">Solicite um orçamento via e-mail!</p>
                <form method="post" action="PHP/form.php">
                    <section>
                        <div>
                            <label for="name">Nome</label>
                            <input type="text" placeholder="Nome" id="name" name="name" required>
                        </div>
                        <div>
                            <label for="mailForm">E-mail</label>
                            <input type="text" placeholder="E-mail" inputmode="email" id="mailForm" name="mailForm" required>
                        </div>
                    </section>
                    <section>
                        <div>
                            <label for="cpf">CPF</label>
                            <input type="text" placeholder="CPF" inputmode="numeric" id="cpf" name="cpf" maxLength="14" required>

                            <script>
                                    const inputCPF = document.getElementById("cpf");
                                    inputCPF.addEventListener("keyup", formatarCPF);

                                    function formatarCPF(e){
                                        var vMaskCPF = e.target.value.replace(/\D/g,"");

                                        vMaskCPF = vMaskCPF.replace(/(\d{3})(\d)/,"$1.$2");
                                        vMaskCPF = vMaskCPF.replace(/(\d{3})(\d)/,"$1.$2");
                                        vMaskCPF = vMaskCPF.replace(/(\d{3})(\d{1,2})$/,"$1-$2");
                                        e.target.value = vMaskCPF;
                                    } 
                            </script>

                        </div>
                        <div>
                            <label for="phone">Telefone/Celular</label>
                            <input type="text" placeholder="Telefone/Celular" inputmode="numeric" id="phone" name="phone" maxLength="16" required>
                            <script>
                                    const inputPhone = document.getElementById("phone");
                                    inputPhone.addEventListener("keyup", formatarPhone);

                                    function formatarPhone(e){
                                        var vMaskPhone = e.target.value.replace(/\D/g,"");

                                        vMaskPhone = vMaskPhone.replace(/(\d{0})(\d)/,"$1($2");
                                        vMaskPhone = vMaskPhone.replace(/(\d{2})(\d)/,"$1) $2");
                                        vMaskPhone = vMaskPhone.replace(/(\d{5})(\d)/,"$1-$2");
                                        vMaskPhone = vMaskPhone.replace(/(\d{4})(\d{1,2})$/,"$1$2");
                                        e.target.value = vMaskPhone;
                                    } 
                            </script>

                        </div>
                        <div>
                            <label for="cep">CEP</label>
                            <input type="text" placeholder="CEP" inputmode="numeric" id="cep" name="cep" maxLength="10" required>
                            
                        </div>
                    </section>
                    <section>
                        <div>
                            <label for="message">Mensagem</label>
                            <textarea name="message" placeholder="Digite aqui" rows="5" required></textarea>
                        </div>
                    </section>
                    <input class="button btBlue" type="submit" name="btSend" value="Enviar Orçamento">
                </form>
            </div>
            <div>
                <div>
                    <p class="tittle">Solicite um orçamento via WhatsApp!</p>
                    <div>
                        <div>
                            <p>Entre em contato conosco para tirar dúvidas ou realizar um orçamento 
                                mais específico. Estamos sempre disponíveis para sanar quaisquer dúvidas
                                 que possam aparecer antes ou depois da realização do orçamento.</p>
                        </div>
                        <div>
                            <div>
                                <div>
                                    <p class="subTittle">Fone:</p>
                                    <p class="tittle">(61) 9 9650-3115</p>
                                </div>
                                <div>
                                    <p class="subTittle">Email:</p>
                                    <p class="tittle">bellaambientes.df@gmail.com</p>
                                </div>
                            </div>
                            <div>
                                <img src="IMG/QrCode.png" />
                                <p>WhatsApp</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer>
            <div>
                <a href="#home">
                    <img src="IMG/icon/Logo v1.svg">
                </a>
                <ul>
                    <li><a href="#home">Início</a></li>
                    <li><a href="#about">Nós</a></li>
                    <li><a href="#product">Produtos</a></li>
                    <li><a href="#contact">Contato</a></li>
                    <li><a href="#contact">Orçamento</a></li>
                </ul>
                <a href="">
                    <img src="IMG/icon/Instagram.svg">
                </a>
            </div>
            <p>Todos os direitos reservados à Bella Casa. <br><br>Desenvolvido por: Onpush LTDA. </p>
        </footer>
<script src="JS/products.js"></script>
<script src="JS/slider1.js"></script>
<script src="JS/slider2.js"></script>
<script src="JS/slider3.js"></script>
<script src="JS/slider31.js"></script>
<script src="JS/slider32.js"></script>
<script src="JS/slider33.js"></script>
<script src="JS/header.js"></script>
<script src="JS/mask.js"></script>
    </main>
    <script>
        const inputCEP = document.getElementById("cep");
        inputCEP.addEventListener("keyup", formatarCEP);

        function formatarCEP(e){
            var vMaskCEP = e.target.value.replace(/\D/g,"");

            vMaskCEP = vMaskCEP.replace(/(\d{2})(\d)/,"$1.$2");
            vMaskCEP = vMaskCEP.replace(/(\d{3})(\d)/,"$1-$2");
            vMaskCEP = vMaskCEP.replace(/(\d{3})(\d{1,2})$/,"$1-$2");
            e.target.value = vMaskCEP;
        } 
    </script>
</body>
</html>